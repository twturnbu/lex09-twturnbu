objdir := obj
objects := $(addprefix $(objdir)/, Bind.o BindList.o BindListList.o Expr.o \
                 ExprList.o Primitives.o REPL.o \
                 interpreter.o)

CC = gcc
CFLAGS = -M -g -Wall -std=c11 

interpreter : $(objects) interpreter.c
	$(CC) $(FLAGS) -o interpreter interpreter.o

interpreter.o :
	$(CC) -c $(FLAGS) interpreter.c 

Bind.o : defs.h


BindList.o : defs.h

BindListList.o : defs.h

Expr.o : defs.h

ExprList.o : defs.h

Primitives.o : defs.h

REPL.o : defs.h

.PHONY: clean
clean:
	rm $(objdir)/*.o	
